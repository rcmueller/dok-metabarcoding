## Bacterial/archaeal life history estimation based on ribosomal RNA (rrn) operon copy numbers

For further details see [rrnDB's website](https://rrndb.umms.med.umich.edu), and if you use their resources, please cite [DOI 10.1093/nar/gku1201](https://doi.org/10.1093/nar/gku1201).

## annOTU.py: query (Z)OTUs assigned to taxa in rrnDB's reference data (subject)

This script queries OTUs assigned to taxa in [rrnDB dumps (NCBI or RDP taxonomy)](https://rrndb.umms.med.umich.edu/static/download/).
[annOTU.py](https://gitlab.com/rcmueller/dok-metabarcoding/-/blob/main/annOTU.py) returns a tab-separated file with (Z)OTUs, taxonomy and hits (mean, stedev, median, number of entries, supporting database).

## Steps to reproduce

```shell
## Optional: download example (Z)OTU-taxa table
wget https://gitlab.com/rcmueller/dok-metabarcoding/-/blob/main/example_OTU_table.tsv

## Download and inflate the latest rrnDB's NCBI and RDP database dumps (v5.8 at time of writing)
wget https://rrndb.umms.med.umich.edu/static/download/rrnDB-5.8_pantaxa_stats_NCBI.tsv.zip
wget https://rrndb.umms.med.umich.edu/static/download/rrnDB-5.8_pantaxa_stats_RDP.tsv.zip
unzip rrnDB-5.8_pantaxa_stats_NCBI.tsv.zip
unzip rrnDB-5.8_pantaxa_stats_RDP.tsv.zip

## Download annOTU.py and execute on NCBI and RDP rrnDB data
## Note: you may have to install pandas library for annOTU.py to work
wget https://gitlab.com/rcmueller/dok-metabarcoding/-/blob/main/annOTU.py
./annOTU.py example_ZOTU_table.tsv rrnDB-5.8_pantaxa_stats_NCBI.tsv
./annOTU.py example_ZOTU_table.tsv rrnDB-5.8_pantaxa_stats_RDP.tsv

## Optional: download/view example result table
wget https://gitlab.com/rcmueller/dok-metabarcoding/-/blob/main/20221128_example_ZOTU_table_to_rrnDB-5.8_pantaxa_stats_NCBI.tsv
