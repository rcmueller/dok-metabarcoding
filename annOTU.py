#!/usr/bin/python3

## 20221128, rmueller, extended to parse rrnDB dump as argument (previously hard-coded)
## 20220405, rmueller, query (Z)OTUs assigned to taxa in rrnDB's NCBI/RDP subject table
## Note: OTU table is poorly formatted (gaps in taxonomic ranks), so special treatment
##     line-by-line while reading the file

import csv
import datetime
import pathlib
import sys
import pandas as pd
import statistics

## Number of arguments and usage
if len(sys.argv) != 3:
    print ("""\

Look up taxa from (Z)OTU table in rrnDB's NCBI or RDP table

Usage:  ./annOTU.py (Z)OTU-table rrnDB-NCBI/RDP-table

""")
    sys.exit(1)


## Argument handling and variables
otuFile = sys.argv[1]
if not pathlib.Path(otuFile).is_file():
    print('\n\tWARNING: file "' + otuFile + '" not found; script terminated\n')
    sys.exit(1)

rrnFile = sys.argv[2]
if not pathlib.Path(rrnFile).is_file():
    print('\n\tWARNING: file "' + rrnFile + '" not found; script terminated\n')
    sys.exit(1)
if 'NCBI' in rrnFile:
    rrnDB = 'NCBI'
elif 'RDP' in rrnFile:
    rrnDB = 'RDP'

currDate = datetime.datetime.now().strftime('%Y%m%d')
resultFile = currDate + '_' + pathlib.Path(otuFile).stem + '_to_' + pathlib.Path(rrnFile).stem + '.tsv'

queryDict = {}
queryData = pd.DataFrame(columns=['otu', 'd', 'p', 'c', 'o', 'f', 'g', 's'])


## Parse (Z)OTU table
print ('\nINFO: parsing (Z)OTU table:\t' + otuFile)
with open(otuFile, 'r') as otuFile:
    # Read tab-delimited file
    reader = csv.reader(otuFile, delimiter="\t")
    # Needs to be handled row-by-row, since there can be gaps in ranks
    for row in reader:
        # List of ranks
        taxaList = row[3].split(',')
        for taxa in taxaList:
            # Get OTU of current row
            queryDict['otu'] = row[0]
            # To dictionary: rank (key) and name (value), separated by ':'
            Rank,Name = taxa.split(':')
            queryDict[Rank] = Name
        # Append (deprecated, use concat?) to dataframe and flush dictionary before next row
        queryData = queryData.append(queryDict, sort=False, ignore_index=True)
        queryDict.clear()

# Change 'Nan' to 'None' (and None for all other missing values)
queryData = queryData.where((pd.notnull(queryData)), None)
# Fix names
queryData = queryData.replace(r'_', ' ', regex=True)
# Prepare new columns
queryData['Mean'] = None
queryData['STDEV'] = None
queryData['Median'] = None
queryData['Entries'] = None
queryData['FoundIn'] = None

# Debug
#print(queryData)

## Parse rrnDB table
print ('INFO: parsing rrnDB table:\t' + rrnFile + '\n')
df = pd.read_csv(rrnFile, delimiter = '\t')
myrrnDB = [list(row) for row in df.values]
myrrnDB.insert(0, df.columns.to_list())

# See discussion on elegantly (?) breaking loops on https://stackoverflow.com/questions/653509/breaking-out-of-nested-loops
def incrementRank():
    # For each DB record in subject table
    for rrnRec in myrrnDB:
        # For each rank from species to kingdom
        highRank = ''
        taxName = ''
        rankList = []
        nameList = []
        for rank in range(7,1,-1):
            if rank == 7 and queryData.iloc[otuRow,rank] is not None:
                highRank = 'species'
                taxName = queryData.iloc[otuRow,rank]
                rankList.append(highRank)
                nameList.append(taxName)
            elif rank == 6 and queryData.iloc[otuRow,rank] is not None:
                highRank = 'genus'
                taxName = queryData.iloc[otuRow,rank]
                rankList.append(highRank)
                nameList.append(taxName)
            elif rank == 5 and queryData.iloc[otuRow,rank] is not None:
                highRank = 'family'
                taxName = queryData.iloc[otuRow,rank]
                rankList.append(highRank)
                nameList.append(taxName)
            elif rank == 4 and queryData.iloc[otuRow,rank] is not None:
                highRank = 'order'
                taxName = queryData.iloc[otuRow,rank]
                rankList.append(highRank)
                nameList.append(taxName)
            elif rank == 3 and queryData.iloc[otuRow,rank] is not None:
                highRank = 'class'
                taxName = queryData.iloc[otuRow,rank]
                rankList.append(highRank)
                nameList.append(taxName)
            elif rank == 2 and queryData.iloc[otuRow,rank] is not None:
                highRank = 'phylum'
                taxName = queryData.iloc[otuRow,rank]
                rankList.append(highRank)
                nameList.append(taxName)
            elif rank == 1 and queryData.iloc[otuRow,rank] is not None:
                highRank = 'superkingdom'
                taxName = queryData.iloc[otuRow,rank]
                rankList.append(highRank)
                nameList.append(taxName)
            ## Core: find query rank-taxa in subject rank-taxa
            if highRank == rrnRec[1] and taxName == rrnRec[2]:
                print ('Found match for', rankList[0], nameList[0], 'in', rrnDB, highRank, taxName)
                # Calculate statistics where needed and add additional entries to result table
                if type(rrnRec[10]) != list: # We might have already converted this to a list earlier (?!)
                    rrnRec[10] = [int(i) for i in rrnRec[10].replace('[', '').replace(']', '').split(', ')] # Convert to list of int and remove crap
                realMean = round(statistics.mean(rrnRec[10]), 2)
                realMedian = round(statistics.median(rrnRec[10]), 2) # Only provided on species level by rrnDB (rrnRec[7]); so, manually calculate for all ranks
                if len(rrnRec[10]) > 1:
                    realSTDEV = round(statistics.stdev(rrnRec[10]), 2)
                else:
                    realSTDEV = None
                queryData.loc[otuRow,'Mean'] = realMean
                queryData.loc[otuRow,'STDEV'] = realSTDEV
                queryData.loc[otuRow,'Median'] = realMedian
                queryData.loc[otuRow,'Entries'] = len(rrnRec[10])
                queryData.loc[otuRow,'FoundIn'] = rrnDB + ';' + rrnRec[1] + ';' + rrnRec[2]
                # Quit function if match!
                return

## Main program: Call function for each line in OTU table
for otuRow in range(len(queryData)):
    incrementRank()
    
## Write result
queryData.to_csv(resultFile, sep = '\t', index = False)
print ('\nINFO: Results written to:\t' + resultFile + '\n')
